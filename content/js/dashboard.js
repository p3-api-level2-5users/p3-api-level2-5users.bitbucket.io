/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8203909138932911, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.9886363636363636, 500, 1500, "findAllFeeGroup"], "isController": false}, {"data": [0.9878048780487805, 500, 1500, "findAllChildActiveSubsidies"], "isController": false}, {"data": [1.0, 500, 1500, "getUpcomingEvents"], "isController": false}, {"data": [0.9634146341463414, 500, 1500, "getListDomainByLevel"], "isController": false}, {"data": [0.9883720930232558, 500, 1500, "getPendingEvents"], "isController": false}, {"data": [1.0, 500, 1500, "getPortfolioTermByYear"], "isController": false}, {"data": [0.425, 500, 1500, "findAllCurrentFeeTier"], "isController": false}, {"data": [0.9880952380952381, 500, 1500, "findAllClassResources"], "isController": false}, {"data": [0.9883720930232558, 500, 1500, "listBulkInvoiceRequest"], "isController": false}, {"data": [0.9659090909090909, 500, 1500, "getAllEvents"], "isController": false}, {"data": [1.0, 500, 1500, "findAllCustomSubsidies"], "isController": false}, {"data": [0.9767441860465116, 500, 1500, "findAllUploadedSubsidyFiles"], "isController": false}, {"data": [0.9880952380952381, 500, 1500, "getAllChildDiscounts"], "isController": false}, {"data": [0.975609756097561, 500, 1500, "getChildFinancialAssistanceStatus"], "isController": false}, {"data": [1.0, 500, 1500, "getMyDownloadPortfolio"], "isController": false}, {"data": [0.9939759036144579, 500, 1500, "getListDomain"], "isController": false}, {"data": [0.9880952380952381, 500, 1500, "getAdvancePaymentReceipts"], "isController": false}, {"data": [0.9883720930232558, 500, 1500, "getLessonPlan"], "isController": false}, {"data": [0.975609756097561, 500, 1500, "findAllProgramBillingUpload"], "isController": false}, {"data": [0.9634146341463414, 500, 1500, "findAllChildHistorySubsidiesForBillingAdjustment"], "isController": false}, {"data": [1.0, 500, 1500, "getChildSemesterEvaluation"], "isController": false}, {"data": [0.9880952380952381, 500, 1500, "getCentreManagementConfig"], "isController": false}, {"data": [1.0, 500, 1500, "updateClassResourceOrder"], "isController": false}, {"data": [0.0, 500, 1500, "getChildPortfolio"], "isController": false}, {"data": [1.0, 500, 1500, "getStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCentreForSchool"], "isController": false}, {"data": [0.9883720930232558, 500, 1500, "findAllAbsentForVoidingSubsidyByChild"], "isController": false}, {"data": [0.9880952380952381, 500, 1500, "portfolioByID"], "isController": false}, {"data": [1.0, 500, 1500, "invoicesByFkChild"], "isController": false}, {"data": [0.9880952380952381, 500, 1500, "getCountStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.5, 500, 1500, "findAllFeeDraft"], "isController": false}, {"data": [0.0, 500, 1500, "findAllConsolidatedRefund"], "isController": false}, {"data": [1.0, 500, 1500, "getPastEvents"], "isController": false}, {"data": [1.0, 500, 1500, "getMyDownloadAlbum"], "isController": false}, {"data": [0.9880952380952381, 500, 1500, "getRefundChildBalance"], "isController": false}, {"data": [0.45121951219512196, 500, 1500, "findAllUploadedGiroFiles"], "isController": false}, {"data": [0.4764705882352941, 500, 1500, "findAllInvoice"], "isController": false}, {"data": [0.9302325581395349, 500, 1500, "getChildChecklist"], "isController": false}, {"data": [1.0, 500, 1500, "getAllArea"], "isController": false}, {"data": [0.9767441860465116, 500, 1500, "bankAccountInfoByIDChild"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCreditDebitNotes"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 1893, 0, 0.0, 738.6080295826719, 3, 13469, 23.0, 2420.000000000001, 3784.699999999999, 12159.14, 6.13727657946525, 306.6029135397383, 10.913973950413853], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["findAllFeeGroup", 44, 0, 0.0, 27.0, 7, 574, 9.0, 39.5, 74.5, 574.0, 0.14997051715969473, 0.13766824817393852, 0.16154050822963212], "isController": false}, {"data": ["findAllChildActiveSubsidies", 41, 0, 0.0, 36.390243902439025, 14, 506, 17.0, 33.400000000000034, 172.39999999999992, 506.0, 0.14571405216563066, 0.16303286695951638, 0.23749682916448986], "isController": false}, {"data": ["getUpcomingEvents", 41, 0, 0.0, 5.195121951219512, 3, 21, 4.0, 6.800000000000004, 11.599999999999994, 21.0, 0.14636479819507214, 0.08533182082271296, 0.21011352865894145], "isController": false}, {"data": ["getListDomainByLevel", 41, 0, 0.0, 82.26829268292684, 12, 1532, 14.0, 28.800000000000047, 842.6999999999992, 1532.0, 0.15059964370328197, 0.7961399371430146, 0.26781440545280905], "isController": false}, {"data": ["getPendingEvents", 43, 0, 0.0, 30.209302325581405, 11, 606, 13.0, 34.40000000000001, 58.799999999999955, 606.0, 0.1580522086877256, 0.07887175648381618, 0.20559134958208056], "isController": false}, {"data": ["getPortfolioTermByYear", 82, 0, 0.0, 9.5609756097561, 3, 53, 9.0, 16.400000000000006, 20.0, 53.0, 0.2913659734288445, 0.29266027016199236, 0.3697559399128745], "isController": false}, {"data": ["findAllCurrentFeeTier", 40, 0, 0.0, 1038.225, 735, 2801, 825.5, 1679.2999999999997, 2750.799999999998, 2801.0, 0.1449023358256535, 1.0667833073867588, 0.36678403755868544], "isController": false}, {"data": ["findAllClassResources", 42, 0, 0.0, 25.952380952380945, 3, 650, 5.0, 12.90000000000002, 131.70000000000005, 650.0, 0.1556368325681189, 0.09134544567718698, 0.21612849210143076], "isController": false}, {"data": ["listBulkInvoiceRequest", 43, 0, 0.0, 69.3720930232558, 25, 1411, 30.0, 51.2, 193.59999999999957, 1411.0, 0.14506492498793935, 0.2161379088806723, 0.21972236196903708], "isController": false}, {"data": ["getAllEvents", 44, 0, 0.0, 98.38636363636361, 24, 1361, 26.0, 113.0, 1030.25, 1361.0, 0.16235563263348216, 1.043578880853105, 0.28983017231836466], "isController": false}, {"data": ["findAllCustomSubsidies", 43, 0, 0.0, 22.55813953488372, 17, 43, 22.0, 29.200000000000003, 33.8, 43.0, 0.1504185483560652, 0.18094840315843969, 0.18802318544508148], "isController": false}, {"data": ["findAllUploadedSubsidyFiles", 43, 0, 0.0, 254.3255813953488, 181, 1562, 205.0, 291.6, 370.9999999999998, 1562.0, 0.15412683563269067, 12.854087783073648, 0.22200886968576047], "isController": false}, {"data": ["getAllChildDiscounts", 42, 0, 0.0, 37.47619047619049, 12, 592, 14.0, 20.400000000000006, 330.3000000000005, 592.0, 0.14555687634943354, 0.08898301229955605, 0.23979926797020934], "isController": false}, {"data": ["getChildFinancialAssistanceStatus", 41, 0, 0.0, 61.90243902439022, 18, 1606, 21.0, 33.800000000000004, 36.699999999999996, 1606.0, 0.14929557975843247, 0.10745199441598119, 0.19580465196833477], "isController": false}, {"data": ["getMyDownloadPortfolio", 42, 0, 0.0, 9.976190476190474, 5, 97, 7.0, 11.700000000000003, 17.250000000000007, 97.0, 0.14453303784356708, 0.0849696179510033, 0.148343967552333], "isController": false}, {"data": ["getListDomain", 83, 0, 0.0, 18.987951807228907, 3, 509, 16.0, 26.200000000000017, 29.599999999999994, 509.0, 0.2894810267857143, 1.2302637100219724, 0.47973224094935824], "isController": false}, {"data": ["getAdvancePaymentReceipts", 42, 0, 0.0, 41.40476190476191, 14, 803, 15.0, 21.400000000000006, 248.55000000000038, 803.0, 0.14766686355581807, 0.22207711901949204, 0.2354882697135263], "isController": false}, {"data": ["getLessonPlan", 43, 0, 0.0, 29.30232558139535, 4, 949, 5.0, 18.0, 40.39999999999996, 949.0, 0.1554141969061732, 0.10123170833634523, 0.24920909308587538], "isController": false}, {"data": ["findAllProgramBillingUpload", 41, 0, 0.0, 66.8780487804878, 22, 810, 24.0, 72.40000000000006, 590.7999999999994, 810.0, 0.1492075622759612, 0.20318797537165417, 0.22249994882362573], "isController": false}, {"data": ["findAllChildHistorySubsidiesForBillingAdjustment", 41, 0, 0.0, 163.78048780487808, 63, 2561, 78.0, 92.60000000000001, 815.4999999999993, 2561.0, 0.1522957658062575, 0.22081144855560464, 0.2738051805169141], "isController": false}, {"data": ["getChildSemesterEvaluation", 42, 0, 0.0, 24.19047619047619, 8, 435, 10.0, 23.10000000000001, 84.20000000000002, 435.0, 0.15345548877400025, 0.11823865297137325, 0.23527843493669962], "isController": false}, {"data": ["getCentreManagementConfig", 42, 0, 0.0, 32.4047619047619, 6, 785, 8.5, 16.10000000000001, 171.75000000000026, 785.0, 0.15069156665410902, 0.17776895753726926, 0.2439908374145633], "isController": false}, {"data": ["updateClassResourceOrder", 43, 0, 0.0, 6.790697674418607, 4, 24, 6.0, 8.0, 15.999999999999972, 24.0, 0.15108871718651729, 0.0891187355279848, 0.14371133841764436], "isController": false}, {"data": ["getChildPortfolio", 41, 0, 0.0, 3885.0731707317073, 3224, 5484, 3654.0, 4714.8, 4897.0, 5484.0, 0.14917553230196037, 0.27655958357286314, 0.3608474546015194], "isController": false}, {"data": ["getStaffCheckInOutRecordsByRole", 42, 0, 0.0, 4.976190476190475, 4, 16, 5.0, 6.0, 6.850000000000001, 16.0, 0.1464986815118664, 0.08769891774099033, 0.26738870678288895], "isController": false}, {"data": ["findAllCentreForSchool", 84, 0, 0.0, 2758.809523809524, 1897, 5089, 2465.0, 3854.0, 4384.25, 5089.0, 0.29031187790311874, 264.20655363403944, 0.7139196229228877], "isController": false}, {"data": ["findAllAbsentForVoidingSubsidyByChild", 43, 0, 0.0, 41.11627906976745, 16, 639, 19.0, 27.0, 258.3999999999992, 639.0, 0.14903026354095905, 0.09328945207983863, 0.19836743086555392], "isController": false}, {"data": ["portfolioByID", 42, 0, 0.0, 70.02380952380952, 23, 1465, 27.5, 82.90000000000002, 136.85000000000005, 1465.0, 0.14828152306307263, 0.23559964650744056, 0.5211574233437484], "isController": false}, {"data": ["invoicesByFkChild", 42, 0, 0.0, 41.83333333333332, 25, 307, 31.0, 60.7, 75.10000000000001, 307.0, 0.15059700957366703, 0.4349378059001757, 0.43767255907346986], "isController": false}, {"data": ["getCountStaffCheckInOutRecordsByRole", 42, 0, 0.0, 29.928571428571434, 3, 998, 5.0, 12.100000000000009, 23.700000000000003, 998.0, 0.14449574425629416, 0.08720543940467754, 0.15790111115507147], "isController": false}, {"data": ["findAllFeeDraft", 43, 0, 0.0, 1041.744186046512, 907, 1182, 1027.0, 1157.2, 1162.6, 1182.0, 0.15485562414019116, 0.08801364575155396, 0.24664992477798026], "isController": false}, {"data": ["findAllConsolidatedRefund", 42, 0, 0.0, 2396.166666666667, 2178, 3408, 2326.0, 2796.3, 2910.75, 3408.0, 0.14696568351289974, 0.20820821931828917, 0.25388895911554654], "isController": false}, {"data": ["getPastEvents", 42, 0, 0.0, 20.476190476190478, 8, 187, 10.0, 52.80000000000007, 126.30000000000011, 187.0, 0.15272449591825604, 0.07576566789694733, 0.19239707005327175], "isController": false}, {"data": ["getMyDownloadAlbum", 42, 0, 0.0, 32.476190476190474, 6, 498, 7.5, 30.100000000000037, 405.9500000000006, 498.0, 0.15228371180670122, 0.09963875674852521, 0.24047144725726158], "isController": false}, {"data": ["getRefundChildBalance", 42, 0, 0.0, 38.833333333333336, 9, 886, 11.0, 15.400000000000006, 259.15000000000043, 886.0, 0.15215296444692397, 0.1451693811178171, 0.2381847675863468], "isController": false}, {"data": ["findAllUploadedGiroFiles", 41, 0, 0.0, 796.560975609756, 535, 2781, 591.0, 1551.6000000000008, 2658.9999999999986, 2781.0, 0.1485222040695084, 42.26643567426364, 0.21248537984553692], "isController": false}, {"data": ["findAllInvoice", 85, 0, 0.0, 6195.482352941175, 24, 13469, 10577.0, 12471.6, 12919.4, 13469.0, 0.2765828674810134, 0.5874081175444648, 0.8892978089756022], "isController": false}, {"data": ["getChildChecklist", 43, 0, 0.0, 428.67441860465124, 278, 1483, 311.0, 1049.8000000000006, 1365.6, 1483.0, 0.14438154333796696, 0.34030590734566285, 0.4754439102886959], "isController": false}, {"data": ["getAllArea", 42, 0, 0.0, 134.47619047619048, 52, 208, 162.5, 185.10000000000002, 204.75000000000003, 208.0, 0.1407035175879397, 0.110415358040201, 0.13232176507537688], "isController": false}, {"data": ["bankAccountInfoByIDChild", 43, 0, 0.0, 91.81395348837208, 29, 1356, 35.0, 70.6, 780.3999999999977, 1356.0, 0.14792085202410765, 0.21630468341497644, 0.34047797677814623], "isController": false}, {"data": ["findAllCreditDebitNotes", 43, 0, 0.0, 3981.7209302325577, 3279, 5820, 3821.0, 4732.0, 5060.399999999999, 5820.0, 0.14966829329416434, 0.28776271897689537, 0.349615778866837], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 1893, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
